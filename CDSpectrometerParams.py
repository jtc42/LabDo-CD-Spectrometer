import numpy as np

# Home positions
NANOHOME = 311.4 # Offset from device home to treat as zero degrees
QWPHOME = 3.4 # Offset from device home to treat as zero degrees

# Angle ranges
nano_angles = np.arange(8, 92, 2)
nano_angles = np.add(nano_angles, NANOHOME)
#nano_angles = [NANOHOME]

qwp_angles = [45]
#qwp_angles = np.arange(0, 365, 5)
qwp_angles = np.add(qwp_angles, QWPHOME)


my_plot_params = {
    'ymax': 20000, 
    'ymin': 1500,
    'xline': 633,
    'yline': 1700,
    'averages': 1
}